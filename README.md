# Jackal

[![pipeline status](https://gitlab.com/bsheffield/jackal/badges/master/pipeline.svg)](https://gitlab.com/bsheffield/jackal/commits/master)

[![coverage report](https://gitlab.com/bsheffield/jackal/badges/master/coverage.svg)](https://gitlab.com/bsheffield/jackal/commits/master)

Jackal robot is configured on top of ROS Kinetic with the Desktop package installed for GUI support(assuming Linux X11 host).

TODO

## Common Errors and Fixes

### Desktop Sharing fail

Remote Sharing can be enabled with the following steps if it crashing from the Gnome GUI Menu

* Add the following key entry into the /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml

```
    <key name='enabled' type='b'>
            <summary>Enable remote access to the desktop</summary>
            <description>If true, allows remote access to the desktop via the RFB protocol.  Users on remote machines may then connect to the desktop using a VNC viewer.</description>
            <default>false</default>
    </key>

```

* Disable the encryption requirement by setting to false.

* Compile the schemas for Gnome

```bash
sudo glib-compile-schemasj /usr/share/glib-2.0/schemas
```

* may have to add /usr/lib/vino/vino-server to .bashrc

### Gazebo fail

```
[ INFO] [1555866596.411793152]: Waiting for valid clock time...
gzclient: symbol lookup error: /usr/lib/aarch64-linux-gnu/libavdevice.so.57: undefined symbol: drmModeFreeFB
[gazebo_gui-3] process has died [pid 11861, exit code 127, cmd /opt/ros/melodic/lib/gazebo_ros/gzclient --verbose __name:=gazebo_gui __log:=/home/nvidia/.ros/log/447fa850-6458-11e9-a531-122259cffb17/gazebo_gui-3.log].
log file: /home/nvidia/.ros/log/447fa850-6458-11e9-a531-122259cffb17/gazebo_gui-3*.log
gzserver: symbol lookup error: /usr/lib/aarch64-linux-gnu/libavdevice.so.57: undefined symbol: drmModeFreeFB
[gazebo-2] process has died [pid 11856, exit code 127, cmd /opt/ros/melodic/lib/gazebo_ros/gzserver --verbose -e ode worlds/ocean_waves.world __name:=gazebo __log:=/home/nvidia/.ros/log/447fa850-6458-11e9-a531-122259cffb17/gazebo-2.log].
log file: /home/nvidia/.ros/log/447fa850-6458-11e9-a531-122259cffb17/gazebo-2*.log
```

Fix for Xavier

```bash
sudo rm /usr/lib/aarch64-linux-gnu/libdrm.so.2
sudo -H ln -s /usr/lib/aarch64-linux-gnu/libdrm.so /usr/lib/aarch64-linux-gnu/libdrm.so.2
```
## Install Tensorflow on Jetson

nvidia@jackal:/media/nvidia/jackal-data/tensorflow-venv/lib/python3.5/site-packages$ ln -s /usr/lib/python3.5/dist-packages/cv2.cpython-35m-aarch64-linux-gnu.so cv2.so

pip3 install --upgrade --proxy 130.109.2.10:8080 --extra-index-url=https://developer.download.nvidia.com/compute/redist/jp33 tensorflow-gpu

## Cheatsheet

  rosrun rosserial_python serial_node.py /dev/ttyUSB0
  rostopic pub nerf_gun/init std_msgs/UInt16 90 --once

### navigate unexplored and bounded area.  stop when area is fully explorered.

roslaunch explore_lite explore.launch
roslaunch jackal_navigation gmapping_demo.launch config:=front_laser
roslaunch jackal_viz view_robot.launch config:=navigation

### Add routes

sudo route del -net 192.168.1.0 gw 0.0.0.0 netmask 255.255.255.0 dev eth0
sudo route add -host 192.168.1.7 dev eth0

## Starting Services Automatically

To automatically start a service such as starting device drivers and other essentials at boot:

```bash
rosrun robot_upstart install jackal_base/launch/base.launch --job jackal_base
```


```bash
#rosrun robot_upstart install jackal_bringup/launch/accessories.launch --job jackal_bringup --setup /home/nvidia/catkin_ws/devel/setup.bash
rosrun robot_upstart install --job jackal_bringup --provider systemd --user nvidia --setup /home/nvidia/catkin_ws/devel/setup.bash /home/nvidia/catkin_ws/src/jackal_bringup/launch/accessories.launch
```

## udev rules

```bash
sudo service udev stop
sudo service udev start
sudo udevadm trigger
rosrun pointgrey_camera_driver list_cameras
```

## Prerequisities 

### Install NVIDIA-Docker

1. Install NVIDIA Linux Driver.

2. Install Docker CE

3. Follow instructions at https://github.com/NVIDIA/nvidia-docker

4. Add host user to ```docker``` group to launch docker as a normal user with admin privileges.

## Getting Started

### Build ROS source packages

```bash
catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/melodic
. /opt/ros/melodic/setup.bash
```

## Running Tensorflow Object Detection

nvidia-docker run -d asdf

*On the host*

```bash
localhost:8888/tree
```

*To test installion*

```python
python object_detection/builders/model_builder_test.py
```

```python
jupyter notebook --ip 0.0.0.0 --no-browser object_detection_tutorial.ipynb --allow-root
```

## Tips and Tricks

### View live camera feed

To launch camera driver and view what camera sees

**Terminal 1:  Launch camera driver(replace usb.launch with specific camera)**
```bash
roslaunch usb.launch #replace with camera driver launch file name
```


**Terminal 2:  View image data on camera topic 2D example**
```bash
rosrun image_view image_view image:=/camera/image_raw
```

**OR**

**Terminal 2: View image data with *find_object_2d*.**
```bash
rosrun find_object_2d find_object_2d image:=/camera/image_raw
```

## Reference

### Common Parameters for Local & Global cost map

```obstacle_range: 6.0``` - the range obstacles will be considered during path planning

```raytrace_range: 8.5``` - defines range in which area could be considered as free

```footprint: [[0.12, 0.14], [0.12, -0.14], [-0.12, -0.14], [-0.12, 0.14]]``` - defines coordinates of robot outline that will be considered during collision detection

```map_topic: /map``` - defines topic where occupancy grid is published

```subscribe_to_updates: true``` - defines if move_base should periodically check if map was updated.

```observation_sources: laser_scan_sensor``` - defines type of sensor used to provide data

```
laser_scan_sensor: {sensor_frame: laser_frame, data_type: LaserScan, 
    topic: scan, marking: true, clearing: true}
```

laser_scan_sensor
  sensor frame:  coordinate frame tied to sensor
  data_type - type of message published by sensor
  topic - name of topic where sensor data is published
  marking - true is sensor can be used to mark area as occupied
  clearing - true if sensor can be used to mark area as clear

```global_frame: map``` - defines coordinate frame tied to occupancy grid map

```robot_base_frame: base_link``` - paramater that defines coordinate frame tied to robot

```always_send_full_costmap: true``` - defines if costmap should be always published with complete data

All parameters put together looks like this for a ```costmap_common_params.yaml```:

```
obstacle_range: 6.0
raytrace_range: 8.5
footprint: [[0.12, 0.14], [0.12, -0.14], [-0.12, -0.14], [-0.12, 0.14]]
map_topic: /map
subscribe_to_updates: true
observation_sources: laser_scan_sensor
laser_scan_sensor: {sensor_frame: laser_frame, data_type: LaserScan, topic: scan, marking: true, clearing: true}
global_frame: map
robot_base_frame: base_link
always_send_full_costmap: true
```

### Parameters for Local Cost Map

```local_costmap: ``` - parameter that groups the following parameters to be considered only by the local planning

```update_frequency: 5``` - defines how often cost should be recalculated

```publish_frequency: 5``` - defines how often cost map should be published to topic

```transform_tolerance: 0.25``` - paramater that defines latency in published transforms(measured in seconds.)  If transforms are older than what this parameter defines, the planner will stop.

```static_map: false``` - defines if map can change in time; true if map will not change.

```rolling_window:  true``` - defines if map should follow position of robot.

```width:  3``` and ```height:  3``` - defines size of map(in meters)

```origin_x:  -1.5``` and ```origin_y:  -1.5``` - parameters that define position of left bottom map corner(in meters.)  If these values are half of map size, and ```rolling window``` is set to true, then the robot will always be in cost map center.

```resolution:  0.1``` - parameter that defines size of single map cell(in meters).

```inflation_radius: 1.0``` - paramter tthat defines distance to obstacle where cost should be considered.  Any further from obstacle than this value will be treated as no cost.

Local Costmap parameters in the local_costmap_params.yaml file would like like the following:

local_costmap:
  update_frequency: 5
  publish_frequency: 5
  transform_tolerance: 0.25
  static_map: false
  rolling_window: true
  width: 3
  height: 3
  origin_x: -1.5
  origin_y: -1.5
  resolution: 0.1
  inflation_radius: 0.6

### Parameters for Global Cost Map

These parameters are used only by global cost map. Parameter meaning is the same as for local cost map, but values may be different.

Your file for global cost map should look like below saved in the ```global_costmap_params.yaml``` file:

```
global_costmap:
  update_frequency: 2.5
  publish_frequency: 2.5
  transform_tolerance: 0.5
  width: 15
  height: 15
  origin_x: -7.5
  origin_y: -7.5
  static_map: true
  rolling_window: true
  inflation_radius: 2.5
  resolution: 0.1
```

### Parameters for Trajectory Planner

```TrajectoryPlannerROS:``` - parameter that groups the following parameters to be considered only by the trajectory planner

```max_vel_x: 0.2``` - This parameter defines maximum linear velocity that will be set by trajectory planner.

```min_vel_x: 0.1``` - This parameter defines minimum linear velocity that will be set by trajectory planner. This should be adjusted to overcome rolling resistance and other forces that may suppress robot from moving.

```max_vel_theta: 0.35``` and ```min_vel_theta: -0.35``` - defines maximum angular velocity that will be set by trajectory planner

```min_in_place_vel_theta: 0.25``` - This parameter defines minimum angular velocity that will be set by trajectory planner. This should be adjusted to overcome rolling resistance and other forces that may suppress robot from moving.

```
acc_lim_theta: 0.25
acc_lim_x: 2.5
acc_lim_Y: 2.5
``` - parameter that defines maximum values of accelerations used by trajectoryu planner

```holonomic_robot: false``` defines if robot is holonomic or not

```meter_scoring: true``` - This parameter defines if cost function arguments are expressed in map cells or meters (if true, meters are considered).

```
xy_goal_tolerance: 0.15``` and ```yaw_goal_tolerance: 0.25``` - These parameters define how far from destination it can be considered as reached. Linear tolerance is in meters, angular tolerance is in radians.

All the parameters put together look like this stored in the file called ```trajectory_planner.yaml```:

```
TrajectoryPlannerROS:
  max_vel_x: 0.2
  min_vel_x: 0.1
  max_vel_theta: 0.35
  min_vel_theta: -0.35
  min_in_place_vel_theta: 0.25

  acc_lim_theta: 0.25
  acc_lim_x: 2.5
  acc_lim_Y: 2.5

  holonomic_robot: false

  meter_scoring: true

  xy_goal_tolerance: 0.15
  yaw_goal_tolerance: 0.25
```

**NOTE:  Remember to adjust cost map and trajectory planner parameters to your robot and area that you want to explore.**

## Change Jetson Power Mode

https://www.jetsonhacks.com/2017/03/25/nvpmodel-nvidia-jetson-tx2-development-kit/

```bash
sudo nvpmodel -m [mode]
```

## Find Object Build Notes


https://gist.github.com/filitchp/5645d5eebfefe374218fa2cbf89189aa

https://github.com/introlab/find-object/issues/45

sudo apt install --assume-yes build-essential cmake git pkg-config unzip ffmpeg qtbase5-dev python-dev python3-dev python-numpy python3-numpy
sudo apt install libhdf5-dev
sudo apt install --assume-yes libgtk-3-dev libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev
sudo apt install --assume-yes libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
sudo apt install --assume-yes libv4l-dev libtbb-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev
sudo apt install --assume-yes libvorbis-dev libxvidcore-dev v4l-utils


## Find Object GPU supported algorithms

FAST, ORB, and SURF

Under Feature2D within the Find_Object_2D GUI, there will be a checkbox for marking the algorithm as gpu.  Example:  "SURF_gpu."

### build opencv w/ gpu

use jetsonhacks scripts to build opencv w/ gpu

### build find object 2d

```bash
catkin_make -j2 VERBOSE=1 -DOpenCV_DIR=/usr/local/share/OpenCV --pkg find_object_2d
```

# Running Locally

This page walks through the steps required to train an object detection model
on a local machine. It assumes the reader has completed the
following prerequisites:

1. The Tensorflow Object Detection API has been installed as documented in the
[installation instructions](installation.md). This includes installing library
dependencies, compiling the configuration protobufs and setting up the Python
environment.
2. A valid data set has been created. See [this page](preparing_inputs.md) for
instructions on how to generate a dataset for the PASCAL VOC challenge or the
Oxford-IIIT Pet dataset.
3. A Object Detection pipeline configuration has been written. See
[this page](configuring_jobs.md) for details on how to write a pipeline configuration.

## Recommended Directory Structure for Training and Evaluation

```
+data
  -label_map file
  -train TFRecord file
  -eval TFRecord file
+models
  + model
    -pipeline config file
    +train
    +eval
```

## Running the Training Job

A local training job can be run with the following command:

```bash
# From the tensorflow/models/research/ directory
PIPELINE_CONFIG_PATH={path to pipeline config file}
MODEL_DIR={path to model directory}
NUM_TRAIN_STEPS=50000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr
```

where `${PIPELINE_CONFIG_PATH}` points to the pipeline config and
`${MODEL_DIR}` points to the directory in which training checkpoints
and events will be written to. Note that this binary will interleave both
training and evaluation.

## Running Tensorboard

Progress for training and eval jobs can be inspected using Tensorboard. If
using the recommended directory structure, Tensorboard can be run using the
following command:

```bash
tensorboard --logdir=${MODEL_DIR}
```

where `${MODEL_DIR}` points to the directory that contains the
train and eval directories. Please note it may take Tensorboard a couple minutes
to populate with data.

TensorFlow Object Detection Model Training
This is a summary of this nice tutorial.

Prerequisites
Install TensorFlow.
Download the TensorFlow models repository.
Annotating images and serializing the dataset
All the scripts mentioned in this section receive arguments from the command line and have help messages through the -h/--help flags. Also check the README from the repo they come from to get more details, if needed.

Install labelImg. This is a Python package, which means you can install it via pip, but the one from GitHub is better. It saves annotations in the PASCAL VOC format.
Annotate your dataset using labelImg.
Use this script to convert the XML files generated by labelImg into a single CSV file.
Use this script to separate the CSV file into two, one with training examples and one with evaluation examples. Images will be selected randomly and there are options to stratify examples by class, making sure that objects from all classes are pesent in both datasets. The usual proportions are 75% to 80% of the annotated objects used for training and the rest for the evaluation dataset.
Use this script to convert the two CSV files (eg. train.csv and eval.csv) into TFRecord files (eg. train.record and eval.record), the a serialized data format that TensorFlow is most familiar with. You'll need a label map for your classes in order to complete this step (some examples). If you don't have one yet, you can generate one from your original CSV file with this script.
Choosing a neural network and preparing the training pipeline...
Download one of the neural network models provided in this page. The ones trained in the COCO dataset are the best ones, since they were also trained on objects.
Provide a training pipeline, which is a config file that usually comes in the tar.gz file downloaded in the previous step. If they don’t come in the tar.gz, they can be found here (they need some tweaking before using, for example, changing number of classes). You can find a tutorial on how to create your own here.
The pipeline config file has some fields that must be adjusted before training is started. Its header describes which ones. Usually, they are the fields that point to the label map, the training and evaluation directories and the neural network checkpoint. In case you downloaded one of the models provided in this page, you should untar the tar.gz file and point the checkpoint path inside the pipeline config file to the "untarred" directory of the model (see this answer for help).
You should also check the number of classes. COCO has 90 classes, but your problem may have more or less.
There are additional parameters that may affect how much RAM is consumed by the training process, as well as the quality of the training, but I won't go over those here.
Training the network
Train the model. This is how you do it locally. Optional: in order to check training progress, TensorBoard can be started pointing its --logdir to the --model_dir of object_detection/model_main.py.
Export the network, like this.
Use the exported .pb in your object detector.
Tips
In the data augmentation section of the training pipeline, some options can be added or removed to try and make the training better. Some of the options are listed here.

## How to convert XML to TF Record

python3 generate_csv.py 
usage: generate_csv.py [-h] xml_dir output_csv
generate_csv.py: error: the following arguments are required: xml_dir, output_csv

python3 generate_csv.py ../Pictures ../Pictures/output.csv

python3 generate_pbtxt.py csv ng_labels.csv ng.pbtxt

python3 generate_train_eval.py

python3 ../scripts/generate_tfrecord.py target/nerfgun_labels.csv target/nerfgun_pb.txt target/ ng_record

PIPELINE_CONFIG_PATH=/media/nvidia/sh3ff_data/ex_terminators_dircup_2019/model/pipeline.config
MODEL_DIR=/media/nvidia/sh3ff_data/ex_terminators_dircup_2019/model_copy
NUM_TRAIN_STEPS=1000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1
python3 object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr

MODEL_PATH=/home/nvidia/ex_terminators_dircup_2019/models/dircup
INPUT_TYPE=image_tensor
PIPELINE_CONFIG_PATH=$MODEL_PATH/pipeline.config
TRAINED_CKPT_PREFIX=$MODEL_PATH/model.ckpt-200006
EXPORT_DIR=$MODEL_PATH/exported_model
python3 export_inference_graph.py \
    --input_type=${INPUT_TYPE} \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --trained_checkpoint_prefix=${TRAINED_CKPT_PREFIX} \
    --output_directory=${EXPORT_DIR}


python3 ex_terminators_dircup_2019/scripts/generate_csv.py xml target_data/annotations/ target_data/annotations/target.csv

python3 ex_terminators_dircup_2019/scripts/generate_pbtxt.py csv target_data/annotations/target.csv target_data/train/target.pbtx

cd target_data/annotations/
python3 ../../ex_terminators_dircup_2019/scripts/generate_train_eval.py target.csv -o ../train

python3 ~/ex_terminators_dircup_2019/scripts/generate_tfrecord.py target_eval.csv target.pbtx ../images/ target_eval.tf

python3 ~/ex_terminators_dircup_2019/scripts/generate_tfrecord.py target_train.csv target.pbtx ../images/ target_train.tf

## convert

mogrify -resize 640x640! Bulldog_640*.jpg

ssh -X nvidia@192.168.1.11 
