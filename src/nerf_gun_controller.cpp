/*
rostopic pub /nerf_gun/init std_msgs/UInt16MultiArray "layout:
  dim: []
  data_offset: 0
data: [2098, 2098]" --once
*/
// %Tag(FULLTEXT)%
// %Tag(ROS_HEADER)%
#include <ros/ros.h>
// %EndTag(ROS_HEADER)%
// %Tag(MSG_HEADER)%
#include <std_msgs/String.h>
#include <std_msgs/UInt16.h>
// %EndTag(MSG_HEADER)%

#include <sstream>
#include <std_msgs/Float64.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int32MultiArray.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/Bool.h>
#include <sensor_msgs/Joy.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <QTransform>
#include <QColor>

ros::Publisher e_stop_pub;
ros::Publisher ng_pub;
ros::Publisher ng_coor;
std_msgs::UInt16 ng_duration;
std_msgs::UInt16MultiArray ng_servo;
std_msgs::Bool troof;

bool readyToFire = true;

double x_offset = 640;
double y_offset = 360;

//double scale_x = 1.4; //1.1 1.25
double scale_x = 1.1;
double scale_y = 0.9;

double pan_origin =  2048;
double tilt_origin = 2048;

void timerCb(const ros::TimerEvent&){
  ROS_ERROR("Reset ReadyToFire to true");
  readyToFire = true;
}

//void calculate_centroid(const std_msgs::Float32MultiArrayPtr &msg){
void calculate_centroid(const std_msgs::UInt16MultiArrayPtr &msg){
    
    auto data = msg->data;
    
    if(data.size() < 0 || data.size() > 4){
      ROS_ERROR("Coordinates message from MCU was not the expected size!");
      return;
    }
          
    unsigned int x_min = msg->data[0];
    unsigned int x_max = msg->data[1];
    unsigned int y_min = msg->data[2];
    unsigned int y_max = msg->data[3];

    ROS_WARN("Received the follow bounding box parameters \nx_min: %u \nx_max: %u \ny_min: %u \ny_max: %u ", x_min, x_max, y_min, y_max);

    int CentroidX = (x_min + x_max)/2;
    int CentroidY = (y_min + y_max)/2;

    int CorrectedX = (CentroidX - x_offset)/scale_x;
    int CorrectedY = (CentroidY - y_offset)/scale_y;

    int ServoX = pan_origin - CorrectedX;
    int ServoY = tilt_origin + CorrectedY;
   
    //bounds check
      if (ServoX < 1000 || ServoX > 4000 || ServoY < 1000 || ServoY > 4000 ){
        ng_servo.data.clear();
        ng_servo.data.push_back(2048);
        ng_servo.data.push_back(2048);
        ng_coor.publish(ng_servo);
        ROS_WARN("False Detection.  Published origin. %u %u", ServoX, ServoY);
      return;
     }

	   ng_servo.data.clear();
	   ng_servo.data.push_back(ServoX);
	   ng_servo.data.push_back(ServoY);
     ng_coor.publish(ng_servo);
     ROS_ERROR("Published SERVO_X:  %u SERVO_Y:  %u", ServoX, ServoY);
}

void stop_navigation(){
    ROS_ERROR("Stopping navigation.");
    troof.data = true;
    e_stop_pub.publish(troof);
 }

 void resume_navigation(){
     ROS_ERROR("Resuming navigation.");
     troof.data = false;
     e_stop_pub.publish(troof);
}

void ng_ack_cb(const std_msgs::String &object){
    ROS_ERROR("Received acknowledgement of completion from the controller.");
    readyToFire = true;
}

//void fireNgCb(const std_msgs::Float32MultiArrayPtr &object){
void fireNgCb(const std_msgs::UInt16MultiArrayPtr &object){
  if (object->data.size() <= 0)
       return;

	if (!readyToFire){
	    ROS_ERROR("Not ready to fire.  Waiting on the nerf gun controller to let us know it is done.");
	    return;
	}

	ROS_ERROR("Found target.");
  readyToFire = false;

	stop_navigation();

//TODO algorithm to collect a list of recently detected objects for an arbitrary time and deciding among them which is optimal since there are a finite amount of targets and multiple images of the same nerf guns
	calculate_centroid(object);

//TODO wait for nerf_gun/result to be published to and timeout if it does not get published to
  ROS_WARN("Sleeping for 5 seconds...");
  ros::Duration(5).sleep();
  resume_navigation();

  //readyToFire = true;

}

int main(int argc, char **argv) {
   ros::init(argc, argv, "nerf_gun_controller");

   ng_duration.data = 3000;

   ros::NodeHandle n;
   std::string param;
   std::string seven_p = "720";
   std::string two_p  = "2k";
   std::string quality = "~quality";
   //n.getParam(quality, param);
   ros::param::param<std::string>(quality, param, seven_p);
   //ROS_ERROR(param);
   if (param == two_p){
     ROS_ERROR("Set NG controller to 2k settings");
     //2208 x 1242 = 2k
     x_offset = 1104;
     y_offset = 621;
     //scale_x = 1.25 * 2;
     scale_x = 1.1 * 2; //original: 1.5
     //0.9 bad for left and right
     //right 1.1 for scale_x
     scale_y = 0.9 * 2; //original 0.9?
  }

   ros::Timer timer = n.createTimer(ros::Duration(6), timerCb);

   ros::Subscriber obj_sub = n.subscribe("/objects", 1000, fireNgCb);
   //ros::Subscriber ng_ack_sub = n.subscribe("/nerf_gun/result", 1, ng_ack_cb);

   //ng_pub = n.advertise<std_msgs::UInt16>("/nerf_gun/init", 5);
   ng_coor = n.advertise<std_msgs::UInt16MultiArray>("/nerf_gun/init", 5);
   e_stop_pub = n.advertise<std_msgs::Bool>("/e_stop", 1);
  ros::spin();
}
