#!/bin/bash

BUCKET_PREFIX=gs://
BUCKET_NAME=daddy-bucket
MODEL_DIR=$BUCKET_NAME/weapon_dataset_rcnn
PIPELINE_CONFIG_PATH=${MODEL_DIR}/train/pipeline.config
PATH_TO_LOCAL_YAML_FILE=$HOME/cloud.yml
DATE_STAMP=`date +%m_%d_%Y_%H_%M_%S`
JOB_NAME=weapon_training_$DATE_STAMP
OUTPUT_PATH=gs://$MODEL_DIR_NAME/$JOB_NAME
REGION=us-east1

cd ~/models/research
# From tensorflow/models/research/
gcloud ai-platform jobs submit training $JOB_NAME \
    --runtime-version 1.12 \
    --job-dir=gs://${MODEL_DIR} \
    --packages dist/object_detection-0.1.tar.gz,slim/dist/slim-0.1.tar.gz,/tmp/pycocotools/pycocotools-2.0.tar.gz \
    --module-name object_detection.model_main \
    --region $REGION \
    --config ${PATH_TO_LOCAL_YAML_FILE} \
    -- \
    --model_dir=gs://${MODEL_DIR} \
    --pipeline_config_path=gs://${PIPELINE_CONFIG_PATH}
