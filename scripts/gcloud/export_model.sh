#!/bin/bash

BUCKET_DIR=gs://dircup-bucket
INPUT_TYPE=image_tensor
PIPELINE_CONFIG_PATH=$BUCKET_DIR/target_model/pipeline.config
TRAINED_CKPT_PREFIX=$BUCKET_DIR/target_model/model.ckpt-200015
EXPORT_DIR=$BUCKET_DIR/target_model_exported

cd ~/models/research
python object_detection/export_inference_graph.py \
    --input_type=${INPUT_TYPE} \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --trained_checkpoint_prefix=${TRAINED_CKPT_PREFIX} \
    --output_directory=${EXPORT_DIR}
