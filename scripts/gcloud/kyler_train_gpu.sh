#!/bin/bash

BUCKET_PREFIX=gs://
BUCKET_NAME=daddy-bucket
MODEL_DIR=$BUCKET_NAME/bulldog_5-14-19
PIPELINE_CONFIG_PATH=${MODEL_DIR}/train/bulldog_pipeline_5-14-19.config
PATH_TO_LOCAL_YAML_FILE=$HOME/cloud.yml
JOB_NAME=bulldog_training
OUTPUT_PATH=gs://$BUCKET_NAME/$JOB_NAME
cd ~/models/research
# From tensorflow/models/research/
gcloud ai-platform jobs submit training object_detection_`date +%m_%d_%Y_%H_%M_%S` \
    --runtime-version 1.12 \
    --job-dir=gs://${MODEL_DIR} \
    --packages dist/object_detection-0.1.tar.gz,slim/dist/slim-0.1.tar.gz,/tmp/pycocotools/pycocotools-2.0.tar.gz \
    --module-name object_detection.model_main \
    --region us-east1 \
    --config ${PATH_TO_LOCAL_YAML_FILE} \
    -- \
    --model_dir=gs://${MODEL_DIR} \
    --pipeline_config_path=gs://${PIPELINE_CONFIG_PATH}
