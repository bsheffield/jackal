#!/usr/bin/env python

#rosrun sound_play say.py "hello world"

import rospy
from std_msgs.msg import Bool
from sensor_msgs.msg import Joy

pub = rospy.Publisher('e_stop', Bool, queue_size=3)

def listener():
        #todo
        rospy.init_node('e_stop_ps4_node', anonymous=True)
        sub = rospy.Subscriber('bluetooth_teleop/joy', Joy, cb)
        rospy.spin()

def cb(data):
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
        #1 is disable
        #3 is enable
        if len(data.buttons) < 1:
                return
        if data.buttons[1] == 1:
                pub.publish(True)
        elif data.buttons[3] == 1:
                pub.publish(False)

if __name__ == '__main__':
        try:
                listener()
        except rospy.ROSInterruptException:
                pass

