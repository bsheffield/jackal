#!/usr/bin/env python

import rospy
from std_msgs.msg import Bool
from std_msgs.msg import Float32MultiArray
from std_msgs.msg import UInt16MultiArray
from sensor_msgs.msg import Joy
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

NG_ORIGIN = int(2048)
ng_array = UInt16Multiarray(data=list(NG_ORIGIN, NG_ORIGIN))

pub = rospy.Publisher('e_stop', Bool, queue_size=3)
ng_pub = rospy.Publisher('/nerf_gun/init', UInt16MultiArray, queue_size=3)

def listener():
	rospy.init_node('e_stop_ps4_node', anonymous=True)
	sub = rospy.Subscriber('bluetooth_teleop/joy', Joy, cb)
	rospy.spin()

def sound_cb():
	soundhandle = SoundClient()
	rospy.sleep(1)

	voice = 'voice_kal_diphone'
	volume = 1.0
	s = 'Object Detected'

	print 'Saying: %s' % s
	print 'Voice: %s' % voice
	print 'Volume: %s' % volume

	rospy.sleep(1)

def sound_listener():
	# Ordered this way to minimize wait time.
	#rospy.init_node('obj_detected_sound', anonymous = True)

	sub = rospy.Subscriber('/objects', Float32MultiArray, sound_cb)	

def cb(data):
	rospy.loginfo(rospy.get_caller_id() + "I heard %s", data)
	#1 is disable
        #2 is fire ng
	#3 is enable
	if len(data.buttons) < 1:
		return
	if data.buttons[1] == 1:
		pub.publish(True)
        elif data.buttons[2] == 1:
            ng_pub.publish(ng_array)
	elif data.buttons[3] == 1:
		pub.publish(False)

if __name__ == '__main__':

	try:
		sound_listener()
		listener()
	except rospy.ROSInterruptException:
		pass
