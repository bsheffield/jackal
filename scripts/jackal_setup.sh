#!/bin/bash

echo "export JACKAL_LASER=1" >> ~/.bashrc
#echo "export JACKAL_BB2=1" >> ~/.bashrc

#rosrun jackal_bringup install

#SICK LMS-111 LIDAR configs
#export JACKAL_LASER=1
#export JACKAL_LASER_HOST="192.168.0.1" #LMS-111 IP ADDRESS
#export JACKAL_LASER_MOUNT=front
#export JACKAL_LASER_OFFSET="0 0 0"
#export JACKAL_LASER_RPY="0 0 0"

#Pointgrey Bumblebee2 Stereocamera configs
#export JACKAL_BB2=1
#export JACKAL_BB2_MOUNT=front
#export JACKAL_BB2_OFFSET="0 0 0"
#export JACKAL_BB2_RPY="0 0 0"
#export JACKAL_BB2_TILT=0
#export JACKAL_BB2_CALIBRATION=0
#export JACKAL_BB2_SERIAL=0


#touch bb2.rules
#KERNEL=="fw*", MODE="0666", GROUP="pgrimaging"

echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc

