#!/bin/bash

git filter-branch -f --env-filter \
"GIT_AUTHOR_NAME='Brandon Sheffield'; GIT_AUTHOR_EMAIL='Brandon.W.Sheffield@Navy.mil'; \
GIT_COMMITTER_NAME='Brandon Sheffield'; GIT_COMMITTER_EMAIL='brandon@bsheffield.com';" HEAD
