/*
  Description:  Used to shoot Evan from Brandon's cube.
  
  rosrun rosserial_python serial_node.py /dev/ttyUSB0i
  rostopic pub nerf_gun/init std_msgs/UInt16 90 --once
rostopic pub /nerf_gun/init std_msgs/UInt16MultiArray "layout:
  dim: []
  data_offset: 0
data: [2048, 2048]" --once
*/

#include <ros.h>
#include <ax12.h>
#include <std_msgs/UInt16.h>
#include <std_msgs/UInt16MultiArray.h>
#include <std_msgs/String.h>

#define TILT 1
#define PAN 2
#define ORIGIN 2048
#define DELAY_MOVE 2

int led_pin  = 0;
int ng_pin   = 6;
int ng_pin2  = 7;
int duration = 3000;

char ack[13] = "fired ng";
std_msgs::String str_msg;

ros::NodeHandle  nh;
ros::Publisher ng_pub("nerf_gun/result", &str_msg);

//Moves turret to a given position
void smooth_position(unsigned int pan_pos, unsigned int tilt_pos){
  unsigned int pan_pos1 = GetPosition(PAN);
  unsigned int tilt_pos1 = GetPosition(TILT);

  if (tilt_pos1<tilt_pos)
  {
    for(tilt_pos1;tilt_pos1<tilt_pos;tilt_pos1++)
    {
     SetPosition(TILT,tilt_pos1);
     delay(DELAY_MOVE); 
    }
  }
  else if (tilt_pos1>tilt_pos)
  {
    for(tilt_pos1;tilt_pos1>tilt_pos;tilt_pos1--)
    {
     SetPosition(TILT,tilt_pos1);
     delay(DELAY_MOVE); 
    }
  }

  if (pan_pos1<pan_pos)
  {
    for(pan_pos1;pan_pos1<pan_pos;pan_pos1++)
    {
     SetPosition(PAN,pan_pos1);
     delay(DELAY_MOVE); 
    }
  }
  else if (pan_pos1>pan_pos)
  {
    for(pan_pos1;pan_pos1>pan_pos;pan_pos1--)
    {
     SetPosition(PAN,pan_pos1);
     delay(DELAY_MOVE); 
    }
  } 
}


//Set the nerf gun to center position after obtaining position
void ng_origin(){
  smooth_position(ORIGIN, ORIGIN);   
}

//Fires the nerf gun by toggling a digital i/o pin and sending acknowledgement.
void fire_ng(){
  digitalWrite(ng_pin, HIGH);  //turn on nerf gun pin
  delay(1000);
  digitalWrite(ng_pin2, HIGH);
  delay(500);
  digitalWrite(ng_pin2, LOW);  //turn off nerf gun pin
  digitalWrite(ng_pin, LOW);  //turn off nerf gun pin
  str_msg.data = ack;
  ng_pub.publish( &str_msg );
}

//Aim the nerf gun by positioning pan/tilt servos.
void aim_ng (const std_msgs::UInt16MultiArray& cmd_msg){
  unsigned int tilt_pos = cmd_msg.data[1];
  unsigned int pan_pos = cmd_msg.data[0];
  smooth_position(pan_pos, tilt_pos);
}

//Handles automation of the nerf gun by receiving pan/tilt servo coordinates and actuating the nerf gun.
void ng_cb( const std_msgs::UInt16MultiArray& cmd_msg){
  digitalWrite(led_pin, HIGH);  //turn on LED 
  aim_ng(cmd_msg);
  fire_ng();
  ng_origin();
  digitalWrite(led_pin, LOW);  //turn off LED
}

ros::Subscriber<std_msgs::UInt16MultiArray> ng_sub("nerf_gun/init", ng_cb);

/**
 * Initialization of Arduino hardware and position of nerf gun.
 */
void setup(){

  dxlInit(1000000); //set baud rate for MX-64 servos
  pinMode(ng_pin, OUTPUT); //set pin as digital output
  pinMode(ng_pin2, OUTPUT);
  ng_origin();
  nh.initNode();
  nh.advertise(ng_pub);
  nh.subscribe(ng_sub);

}

void loop(){
  nh.spinOnce();
  delay(1000);  
}
